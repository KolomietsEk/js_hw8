let title = document.createElement('title');
title.innerHTML = 'Call to Action 3';

let metaUtf8 = document.createElement('meta');
metaUtf8.setAttribute('charset', 'UTF-8');

document.head.appendChild(title);
document.head.appendChild(metaUtf8);
document.documentElement.setAttribute('lang', 'en');

let h1 = document.createElement('h1');
h1.innerHTML = 'Choose Your Option';
document.body.appendChild(h1);
h1.style.textAlign = 'center';
h1.style.fontFamily = 'Arvo';
h1.style.fontWeight = '400';
h1.style.fontSize = '36px';
h1.style.lineHeight = '48px';
h1.style.color = '#212121';
h1.style.marginTop = '122px';
h1.style.marginBottom = '10px';


let p_sup = document.createElement('p');
p_sup.innerHTML = 'But I must explain to you how all this mistaken idea of denouncing ';
document.body.appendChild(p_sup);
p_sup.style.textAlign = 'center';
p_sup.style.fontFamily = 'OpenSans';
p_sup.style.fontSize = '14px';
p_sup.style.lineHeight = '26px';
p_sup.style.color = '#9FA3A7';
p_sup.style.marginBottom = '55px';


//----------//

let container = document.createElement("div");
//container.classList.add('container');
document.body.appendChild(container);

container.style.display = 'flex';
container.style.justifyContent = 'center';
container.style.marginBottom = '139px';

let block1 = document.createElement('div');
container.appendChild(block1);
block1.classList.add('block1');
block1.style.display = 'flex';
block1.style.flexDirection = 'column';
block1.style.alignItems = 'center';
block1.style.width = '401px';
block1.style.border = '1px solid #E8E9ED';
block1.style.borderTopLeftRadius = '5px';
block1.style.borderBottomLeftRadius = '5px';

let style = document.createElement('style');
style. innerHTML = `
    .block1 {
        transition: 1s;
    }
    .block1:hover {
        box-shadow: 0px 5px 10px 0px rgb(0 0 0 / 50%);
       }
`
document.body.appendChild(style);

let sup = document.createElement('span');
sup.innerHTML = 'FREELANCER';
block1.appendChild(sup); 
sup.classList.add('sup_block1');
sup.style.fontFamily = 'Montserrat';
sup.style.fontWeight = '700';
sup.style.fontSize = '12px';
sup.style.lineHeight = '15px';
sup.style.color = '#9FA3A7';
sup.style.marginTop = '80px';


let h2 = document.createElement('h2');
h2.innerHTML = 'Initially designed to';
block1.appendChild(h2); 
h2.classList.add('h2_block1');
h2.style.fontFamily = 'Arvo';
h2.style.fontWeight = '400';
h2.style.fontSize = '36px';
h2.style.lineHeight = '46px';
h2.style.maxWidth = '210px';
h2.style.textAlign = 'center';
h2.style.marginTop = '19px';
h2.style.color = '#212121';
h2.style.marginBottom = '0px';

let p_block1 = document.createElement('p');
p_block1.innerHTML = 'But I must explain to you how all this mistaken idea of denouncing';
block1.appendChild(p_block1); 
p_block1.classList.add('p_block1');
p_block1.style.fontFamily = 'OpenSans';
p_block1.style.fontSize = '12px';
p_block1.style.lineHeight = '22px';
p_block1.style.textAlign = 'center';
p_block1.style.color = '#9FA3A7';
p_block1.style.maxWidth = '210px';
p_block1.style.marginTop = '25px';
p_block1.style.marginBottom = '52px';

let btn_block1 = document.createElement('button');
btn_block1.innerText = 'START HERE';
block1.appendChild(btn_block1);
btn_block1.classList.add('btn_block1');
btn_block1.style.width = '147px';
btn_block1.style.height = '46px';
btn_block1.style.borderRadius = '150px';
btn_block1.style.border = '3px solid #FFC80A';
btn_block1.style.backgroundColor = '#ffff';
btn_block1.style.fontFamily = 'Montserrat';
btn_block1.style.fontWeight = '700';
btn_block1.style.fontSize = '12px';
btn_block1.style.lineHeight = '15px';
btn_block1.style.marginBottom = '90px';
btn_block1.style.cursor = 'pointer';



//-------//
let block2 = document.createElement('div');
container.appendChild(block2);
block2.classList.add('block2');
block2.style.backgroundColor = '#8F75BE';

block2.style.display = 'flex';
block2.style.flexDirection = 'column';
block2.style.alignItems = 'center';
block2.style.width = '401px';
block2.style.borderTopRightRadius = '5px';
block2.style.borderBottomRightRadius = '5px';

let style2 = document.createElement('style');
style2. innerHTML = `
    .block2 {
        transition: 1s;
    }
    .block2:hover {
        box-shadow: 0px 5px 10px 0px rgb(0 0 0 / 50%);
       }
`
document.body.appendChild(style2);


let sup2 = document.createElement('span');
sup2.innerHTML = 'STUDIO';
block2.appendChild(sup2); 
sup2.style.fontFamily = 'Montserrat';
sup2.style.fontWeight = '700';
sup2.style.fontSize = '12px';
sup2.style.lineHeight = '15px';
sup2.style.color = '#FFC80A';
sup2.style.marginTop = '80px';

let h2_block2 = document.createElement('h2');
h2_block2.innerHTML = 'Initially designed to';
block2.appendChild(h2_block2); 
h2_block2.style.fontFamily = 'Arvo';
h2_block2.style.fontWeight = '400';
h2_block2.style.fontSize = '36px';
h2_block2.style.lineHeight = '46px';
h2_block2.style.maxWidth = '210px';
h2_block2.style.textAlign = 'center';
h2_block2.style.marginTop = '19px';
h2_block2.style.color = '#ffff';
h2_block2.style.marginBottom = '0px';

let p_block2 = document.createElement('p');
p_block2.innerHTML = 'But I must explain to you how all this mistaken idea of denouncing';
block2.appendChild(p_block2); 
p_block2.style.fontFamily = 'OpenSans';
p_block2.style.fontSize = '12px';
p_block2.style.lineHeight = '22px';
p_block2.style.textAlign = 'center';
p_block2.style.color = '#9FA3A7';
p_block2.style.maxWidth = '210px';
p_block2.style.marginTop = '25px';
p_block2.style.color = '#ffff';
p_block2.style.marginBottom = '52px';

let btn_block2 = document.createElement('button');
btn_block2.innerText = 'START HERE';
block2.appendChild(btn_block2);
btn_block2.style.width = '147px';
btn_block2.style.height = '46px';
btn_block2.style.borderRadius = '150px';
btn_block2.style.border = '3px solid #FFC80A';
btn_block2.style.backgroundColor = '#8F75BE';
btn_block2.style.fontFamily = 'Montserrat';
btn_block2.style.fontWeight = '700';
btn_block2.style.fontSize = '12px';
btn_block2.style.lineHeight = '15px';
btn_block2.style.color = '#fff';
btn_block2.style.marginBottom = '90px';
btn_block2.style.cursor = 'pointer';
